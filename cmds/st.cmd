#- Loading of all E3 modules and versions
require essioc
require adsis8300
require busy
require mrfioc2

#- load the adsis8300 setup commands
iocshLoad("./iocsh/adsis8300.iocsh")

#- load the autosave module
iocshLoad("$(essioc_DIR)/common_config.iocsh", "IOCNAME=$(PREFIX),AS_TOP=$(E3_IOCSH_TOP)/cmds/autosave")

#- run IOC
iocInit

